# Run
```sh
poetry install
poetry run start
```

# query
```sh
curl localhost:8000/combineddata/1  | jq

{
  "id": "315b338a-1ea3-4116-8502-71843fb36a0c",
  "carbon_info": {
    "id": 1,
    "meat": "broiler",
    "source_title": "Kariniemen sankarit - hiilijalanjälki",
    "source_author": "VTT",
    "year": 2020,
    "country": "Finland",
    "footprint": 2.4,
    "unit": "kg CO2e/kg elopaino",
    "comment": "",
    "source_link": null
  },
  "water_info": {
    "farm_id": 1,
    "meat_type": "broiler",
    "animals": 455000,
    "water_use": 3035.5,
    "water_drinkable_use": 2825.5,
    "water_wash_etc_use": 210
  },
  "weight": 0.4,
  "carbonfootprint": 0.96
}

```
