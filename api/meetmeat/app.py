from fastapi import FastAPI
from pydantic import BaseModel, UUID4, HttpUrl
from uuid import uuid4
from enum import Enum
import csv
from typing import Optional, Union
app = FastAPI()

class PropertyBaseModel(BaseModel):
    """
    Workaround for serializing properties with pydantic until
    https://github.com/samuelcolvin/pydantic/issues/935
    is solved
    """
    @classmethod
    def get_properties(cls):
        return [prop for prop in dir(cls) if isinstance(getattr(cls, prop), property) and prop not in ("__values__", "fields")]

    def dict(
        self,
        *,
        include: Union['AbstractSetIntStr', 'MappingIntStrAny'] = None,
        exclude: Union['AbstractSetIntStr', 'MappingIntStrAny'] = None,
        by_alias: bool = False,
        skip_defaults: bool = None,
        exclude_unset: bool = False,
        exclude_defaults: bool = False,
        exclude_none: bool = False,
    ) -> 'DictStrAny':
        attribs = super().dict(
            include=include,
            exclude=exclude,
            by_alias=by_alias,
            skip_defaults=skip_defaults,
            exclude_unset=exclude_unset,
            exclude_defaults=exclude_defaults,
            exclude_none=exclude_none
        )
        props = self.get_properties()
        # Include and exclude properties
        if include:
            props = [prop for prop in props if prop in include]
        if exclude:
            props = [prop for prop in props if prop not in exclude]

        # Update the attribute dict with the properties
        if props:
            attribs.update({prop: getattr(self, prop) for prop in props})

        return attribs

class MeatType(str, Enum):
    beef = 'beef'
    pork = 'pork'
    broiler = 'broiler'

"""
Have unique UUIDs for each packet.
Solves the issue of same batch being distributed to multiple stores.
"""
class CarbonInfo(BaseModel):
    id: int
    meat: MeatType
    source_title: str
    source_author: str
    year: int
    country: str
    footprint: float
    unit: str = "kg CO2e/kg elopaino"
    # Comment essentially contains Human readable format of the type
    comment: str
    source_link: Optional[HttpUrl]

    @classmethod
    def list_from_csv(cls):
        with open("hkscanmaterials/Litterature survey of carbon footprints of meat.csv", 'r', encoding='ISO-8859-1') as f:
            r = csv.reader(f, delimiter=';', quotechar='"')
            meattype = None
            for row in r:
                id,title,author,year,country,result,unit,comment,link = row
                if row[1:] == [''] * 8:
                    # Meattype rows
                    meattype = row[0].lower()
                    continue
                if id.isdigit():
                    # Actual value row
                    yield cls(
                        id = id,
                        meat = meattype,
                        source_title = title,
                        source_author = author,
                        year = year,
                        country = country,
                        footprint = float(result.replace(',','.')),
                        unit = unit,
                        comment = comment,
                        source_link = link if link else None
                    )
                

    @classmethod
    def from_comment(cls, com: str):
        types = cls.list_from_csv()
        for t in types:
            if t.comment == com:
                return t
        else:
            raise ValueError(f"No CarbonInfo with comment '{com}'")

    @classmethod
    def from_meatid(cls, meat: str, id: int):
        types = cls.list_from_csv()
        for t in types:
            if t.meat == meat and t.id == id:
                return t
        else:
            raise ValueError("No carboninfo for given type and id")

class WaterInfo(BaseModel):
    farm_id: int
    meat_type: MeatType
    animals: int
    water_use: Optional[float]
    water_drinkable_use: Optional[float]
    water_wash_etc_use: Optional[float]
    @classmethod
    def list_from_csv(cls):
        with open("hkscanmaterials/Water footprint _ water use in farms .csv", 'r', encoding='ISO-8859-1') as f:
            r = csv.reader(f, delimiter=';', quotechar='"')
            meattype = None
            for row in r:
                # Skip unwanted lines
                if not row[0].isdigit(): continue
                print(row)
                print(len(row))
                farm_id,meat_type,animals,water_use,water_drinkable,water_wash_etc,_,_,_,_ = row
                if farm_id.isdigit():
                    # Actual value row
                    yield cls(
                        farm_id = farm_id,
                        meat_type = meat_type.replace('*',''),
                        animals=animals,
                        water_use=float(water_use.replace(',','.')),
                        water_drinkable_use=float(water_drinkable.replace(',','.')),
                        water_wash_etc_use=float(water_wash_etc.replace(',','.'))
                    )

    @classmethod
    def from_farm(cls, farm_id):
        types = cls.list_from_csv()
        for t in types:
            if t.farm_id == farm_id:
                return t
        else:
            raise ValueError("No carboninfo for given type and id")


class PackageData(PropertyBaseModel):
    id: UUID4
    carbon_info: CarbonInfo
    water_info: WaterInfo
    weight: float

    @property
    def carbonfootprint(self) -> float:
        return self.carbon_info.footprint * self.weight



@app.get("/combineddata/{package_id}")
def get_history():
    farm_id = 1 # Mock, would be got from DB
    carbon_category_commend = "" # Would be got from DB
    return PackageData(
        id=uuid4(),
        water_info = WaterInfo.from_farm(farm_id),
        carbon_info = CarbonInfo.from_comment(carbon_category_commend),
        weight=0.4 # kg
    )

def main():
    import uvicorn
    uvicorn.run("meetmeat.app:app", host="0.0.0.0", port=8000, reload=True)

if __name__ == "__main__":
    main()

