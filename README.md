# Meet Meat
Scan food products with AR user interface and see their stories.  
Enables you to handle your ecological impact with CO2 and water footprints.

See demo [here](https://youtu.be/YErdgfhP4pU).

# Technologies
AR.js and FastAPI with HKScan water and CO2 data.

# Hackjunction 2020 HKScan track